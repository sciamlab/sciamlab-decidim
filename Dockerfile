ARG ruby_version=2.7.1

FROM ruby:${ruby_version}
LABEL maintainer="ad@sciamlab.com"

ARG decidim_version_branch=release/0.24-stable

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

WORKDIR /code

RUN apt-get install -y git imagemagick wget \
  && apt-get clean

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - \
  && apt-get install -y nodejs \
  && apt-get clean

RUN npm install -g npm@6.3.0

RUN gem install bundler --version '>= 2.1.4' \
  && gem install specific_install \
  && gem specific_install https://github.com/decidim/decidim.git -b ${decidim_version_branch}

RUN decidim .

RUN bundle check || bundle install
RUN bundle exec rake assets:precompile

ENV RAILS_ENV=production
ENV RAILS_SERVE_STATIC_FILES=true

EXPOSE 3000

ENTRYPOINT []

CMD ["bundle", "exec", "rails", "s", "-b", "0.0.0.0"]

